# Sharedex (temporary name)

This is a webapp for creating curated lists (e.g. awesome-* lists on GitHub).
The server is written in Scala; PostgreSQL is used for the database.

## Running

First, ensure a `postgres` instance is running on location specified in
`application.conf`. A `docker-compose` file is provided to easily start one up.
Then, run `server/re-start` in SBT.

## License

Copyright 2017 Bryan Tan

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this project except in compliance with the License. You may obtain a copy of the
License at

> http://www.apache.org/licenses/LICENSE-2.0.

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
