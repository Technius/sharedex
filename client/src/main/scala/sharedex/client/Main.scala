package sharedex.client

import scala.scalajs.js
import org.scalajs.dom

object Main extends js.JSApp {
  def main(): Unit = {
    println("Hello world from the console!")
    val app = dom.document.getElementById("app")
    val text = dom.document.createElement("p")
    text.textContent = "Hello world!"
    app.appendChild(text)
  }
}
