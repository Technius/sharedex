import doobie.imports._
import doobie.scalatest.imports._
import org.scalatest._
import fs2.Task

import sharedex.models._
import sharedex.services._

class QueryTest extends FunSuite with Matchers with QueryChecker {
  val transactor = DriverManagerTransactor[IOLite](
    "org.postgresql.Driver",
    "jdbc:postgresql://127.0.0.1:5432/",
    "postgres",
    ""
  )

  test("UserService.queryUsers") { check(UserService.queryUsers) }
  test("UserService.queryUserId") { check(UserService.queryUserId(1)) }
  test("UserService.queryInsert") { check(UserService.queryInsert("foo")) }
  test("UserService.changeUsername") {
    check(UserService.queryUpdateUsername(1, "foo"))
  }

  test("CurationService.queryInfo") { check(CurationService.queryInfo) }
  test("CurationService.queryEntry") { check(CurationService.queryEntry) }
  test("CurationService.queryFindEntry") { check(CurationService.queryFindEntry(1)) }
  test("CurationService.queryFindInfo") { check(CurationService.queryFindInfo(1)) }
  test("CurationService.queryListAll") { check(CurationService.queryListAll) }
  test("CurationService.queryInsertInfo") { check(CurationService.queryInsertInfo("test", 1, PrivacyLevel.Public)) }
  test("CurationService.queryInsertEntry") {
    check(CurationService.queryInsertEntry(1, "http://example.com/", None, None))
    check(CurationService.queryInsertEntry(1, "http://example.com/", Some("name"), None))
    check(CurationService.queryInsertEntry(1, "http://example.com/", None, Some("desc")))
  }
  test("CurationService.queryInfoForUser") { check(CurationService.queryInfoForUser(Some(1))) }
}
