import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.{ Cookie, `Set-Cookie` }
import com.softwaremill.session.{ SessionConfig, SessionUtil }
import com.softwaremill.session.SessionOptions._
import com.softwaremill.session.SessionDirectives._
import io.circe.generic.auto._
import io.circe.parser._
import org.scalatest._

import sharedex.server.Routes
import sharedex.models._

class RouteTest extends FunSuite with IntegrationTest with Matchers with ScalatestRouteTest {

  val routes = new Routes(transactor,
                          SessionConfig.default(SessionUtil.randomServerSecret())).default

  def checkRoute(route: String) =
    test(s"Route exists: $route") {
      Get(route) ~> routes ~> check {
        status !== StatusCodes.NotFound
      }
    }

  checkRoute("/api/users")
  checkRoute("/api/users/login")
  checkRoute("/api/users/logout")
  checkRoute("/api/users/register")
  checkRoute("/api/curations")

  def apiResult: ApiResponse =
    parse(responseAs[String]).flatMap(_.as[ApiResponse]).toOption.get

  test("User registration works properly") {
    Post("/api/users/register?username=TestUser") ~> routes ~> check {
      status === StatusCodes.Created
      apiResult shouldBe a [ApiResponse.UserCreated]
    }
  }

  def withAuthCookie(block: (User, Cookie) => Any) =
    Post("/api/users/login?id=1") ~> routes ~> check {
      import akka.http.scaladsl.model.headers.{ Cookie, `Set-Cookie` }
      val ApiResponse.UserLoggedIn(user) = apiResult
      val authCookie = header("Set-Cookie").collect { case h: `Set-Cookie` => h.cookie }.get
      block(user, Cookie(authCookie.name -> authCookie.value))
    }

  def checkWithLogin(req: HttpRequest)(block: User => Any) =
    withAuthCookie { (user, authCookie) =>
      req ~> authCookie ~> routes ~> check {
        block(user)
      }
    }

  test("Can login newly registered user") {
    Post("/api/users/login?id=1") ~> routes ~> check {
      status === StatusCodes.OK
      val ApiResponse.UserLoggedIn(user) = apiResult
    }
  }

  test("Can create curations when logged in") {
    List(PrivacyLevel.Public, PrivacyLevel.Private) foreach { privacy =>
      checkWithLogin(Post(s"/api/curations?name=Test_${privacy.id}&privacy=${privacy.id}")) { user =>
        status === StatusCodes.Created
        apiResult shouldBe a [ApiResponse.CurationCreated]
      }
    }
  }

  test("POST /api/curations rejects invalid parameters") {
    withAuthCookie { (user, authCookie) =>
      List(("", "public"), ("asdf", "error")) foreach { case (name, privacy) =>
        Post(s"/api/curations?name=$name&privacy=$privacy") ~> authCookie ~> routes ~> check {
          status === StatusCodes.BadRequest
        }
      }
    }
  }

  test("Private curations are not viewable by anonymous users") {
    Get("/api/curations") ~> routes ~> check {
      status === StatusCodes.OK
      val ApiResponse.CurationListing(xs) = apiResult
      xs foreach { c =>
        c.privacy !== PrivacyLevel.Private
      }
    }
  }

  test("Curation owners can add entries") {
    withAuthCookie { (user, authCookie) =>
      val uri = new java.net.URL("http://example.com").toURI.toString
      val additionalParams =
        List(
          "name=Foo",
          "name=Foo&description=Test",
          "description=Test"
        )
      additionalParams foreach { params =>
        Post(s"/api/curations/1?link=$uri&$params") ~> authCookie ~> routes ~> check {
          status === StatusCodes.Created
        }
      }
    }
  }

  test("Invalid curation entry requests are rejected") {
    withAuthCookie { (user, authCookie) =>
      val uri = new java.net.URL("http://example.com").toURI.toString
      val paramList =
        List(
          "link=invaliduri",
          s"link=$uri&name=",
          s"link=$uri&name=&description=",
          s"link=$uri&description="
        )
      paramList foreach { params =>
        Post(s"/api/curations/1?$params") ~> authCookie ~> routes ~> check {
          status === StatusCodes.BadRequest
        }
      }
    }
  }
}
