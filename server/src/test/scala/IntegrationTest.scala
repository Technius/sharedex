import com.typesafe.config.ConfigFactory
import doobie.imports._
import fs2._
import pureconfig._
import scala.util.Right

import sharedex.server.{ AppConfig, Migrations }

trait IntegrationTest {
  val Right(testCfg) = loadConfig[AppConfig](ConfigFactory.load("test"))
  val transactor = DriverManagerTransactor[Task](
    "org.postgresql.Driver",
    "jdbc:postgresql://127.0.0.1:5432/test",
    "postgres",
    ""
  )

  import java.util.logging
  logging.Logger.getLogger("org.flywaydb").setLevel(logging.Level.WARNING)
  private[this] val migrations = new Migrations(testCfg.db)
  migrations.clean()
  migrations.migrate()
}
