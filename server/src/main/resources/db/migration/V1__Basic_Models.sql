/* sharedex.models.User */
create table "users" (
	"id" serial PRIMARY KEY,
	"username" text NOT NULL
);

/* sharedex.models.Curation */
create table "curations" (
	"id" serial PRIMARY KEY,
	"name" text NOT NULL,
	"owner" int NOT NULL,
	"privacy" text NOT NULL,
  foreign key (owner) references users(id)
);

/* sharedex.models.CurationEntry */
create table "curation_entries" (
	"id" int NOT NULL,
	"link" text NOT NULL,
	"name" text,
	"description" text,
  foreign key (id) references curations(id)
);
