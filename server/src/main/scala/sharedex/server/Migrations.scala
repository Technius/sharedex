package sharedex.server

import com.typesafe.config.Config
import org.flywaydb.core.Flyway
import scala.util.Try

class Migrations(config: DatabaseConfig) {

  private[this] val flyway = {
    val f = new Flyway
    val host = config.host
    val port = config.port
    val name = config.name.getOrElse("")
    val url = s"jdbc:postgresql://$host:$port/$name"
    val password = if (config.password == "") null else config.password
    f.setDataSource(url, config.user, password)
    f.setBaselineOnMigrate(true)
    f
  }

  def migrate(): Unit = flyway.migrate()

  def repair(): Unit = flyway.repair()

  def clean(): Unit = flyway.clean()

  def printInfo(): Unit = {
    flyway.info().all() foreach { i =>
      println(s"${i.getScript()}")
      println(s"Type: ${i.getType().toString}")
      println(s"Version: ${i.getVersion().toString}")
      println(s"Description: ${i.getDescription()}")
    }
  }

  def validate(): Try[Unit] = Try(flyway.validate())
}
