package sharedex.server

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.softwaremill.session.{ SessionConfig, SessionUtil }
import com.typesafe.config.{ Config, ConfigFactory }
import doobie.imports._
import fs2.Task
import pureconfig._
import scala.util.{ Left, Right, Failure, Success }

import sharedex.services._

object SharedexServer {
  def main(args: Array[String]): Unit = {
    val config = loadConfig[AppConfig](ConfigFactory.load())

    config match {
      case Left(errors) =>
        println("The following errors occurred:")
        errors.toList.foreach { e =>
          val locStr = e.location.map(l => s"line ${l.lineNumber}: ").getOrElse("")
          val pathStr = e.path.map(_ + ": ").getOrElse("")
          println(locStr + pathStr + e.description)
        }
      case Right(cfg) => runCommand(cfg, args.toList)
    }
  }

  def runCommand(config: AppConfig, args: List[String]): Unit = {
    val migrations = new Migrations(config.db)
    args match {
      case "--migrate" :: Nil =>
        migrations.migrate()
      case "--migrate" :: "repair" :: _ =>
        migrations.repair()
      case "--migrate" :: "clean" :: _ =>
        migrations.clean()
      case "--migrate" :: "validate" :: _ =>
        migrations.validate() recover {
          case e: Exception => e.printStackTrace()
        }
      case "--migrate" :: "info" :: _ =>
        migrations.printInfo()
      case "--migrate" :: _ =>
        println("Unknown command.")
      case _ =>
        if (config.db.automigrate) {
          migrations.migrate()
        }
        startServer(config)
    }
  }

  def startServer(config: AppConfig): Unit = {
    implicit val system = ActorSystem("sharedex-server")
    implicit val materializer = ActorMaterializer()
    implicit val executionCtx = system.dispatcher

    val dbHost = config.db.host
    val dbPort = config.db.port
    val jdbcUrl = s"jdbc:postgresql://$dbHost:$dbPort/"

    val dbXa = DriverManagerTransactor[Task](
      "org.postgresql.Driver",
      jdbcUrl,
      config.db.user,
      config.db.password
    )

    val sessionConfig = SessionConfig.default(
      config.server.secret.getOrElse(SessionUtil.randomServerSecret()))

    val routes = new Routes(dbXa, sessionConfig)

    val host = config.server.host
    val port = config.server.port

    val bindFut = Http().bindAndHandle(routes.default, host, port = port)

    println(s"Server listening at $host:$port")
  }
}
