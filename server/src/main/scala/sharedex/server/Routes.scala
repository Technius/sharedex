package sharedex.server

import akka.http.scaladsl.model._
import akka.http.scaladsl.server._
import akka.http.scaladsl.server.Directives._
import cats.implicits._
import cats.data.{ EitherT, OptionT }
import com.softwaremill.session.{ SessionConfig, SessionManager }
import com.softwaremill.session.SessionDirectives._
import com.softwaremill.session.SessionOptions._
import doobie.imports._
import doobie.util.transactor.Transactor
import fs2.Task
import fs2.interop.cats._
import scala.util.{ Success, Failure, Try, Left, Right, Either }

import sharedex.models._
import sharedex.services._
import sharedex.services.{ Combinators => Valid }

class Routes(dbXa: Transactor[Task], sessionConfig: SessionConfig)
    extends ApiMarshalling with TaskSupport {

  implicit val sessionManager = new SessionManager[Int](sessionConfig)

  val default: Route =
    logRequest("request") {
      encodeResponse {
        pathPrefix("api") {
          pathPrefix("users") {
            userRoute
          } ~
          pathPrefix("curations") {
            curationRoute
          }
        } ~
        pathPrefix("public") {
          getFromResourceDirectory("public")
        } ~
        pathSingleSlash {
          getFromResource("public/index.html")
        }
      }
    }

  val userRoute: Route =
    (path("login") & post) {
      parameter('id) { idStr => // TODO: passwords, etc.
        val optTask: OptionT[Task, User] = for {
          id <- OptionT.fromOption[Task](Try(idStr.toInt).toOption)
          user <- OptionT(UserService.find(id).transact(dbXa))
        } yield user

        onSuccess(optTask.value) {
          case Some(user) =>
            setSession(oneOff, usingCookies, user.id)(complete(ApiResponse.UserLoggedIn(user)))
          case _ => complete(ApiResponse.Error.loginFail)
        }
      }
    } ~
    (path("register") & post) {
      (parameter('username)) { name =>
        complete {
          UserService.create(name).transact(dbXa).map(ApiResponse.UserCreated(_))
        }
      } ~
      complete {
        ApiResponse.Error.invalidRequest("missing username parameter")
      }
    } ~
    (path("logout") & post) {
      requiredSession(oneOff, usingCookies) { _ =>
        invalidateSession(oneOff, usingCookies) {
          complete(ApiResponse.UserLoggedOut)
        }
      }
    } ~
    pathEnd {
      get {
        complete {
          UserService.list.transact(dbXa).map(ApiResponse.UserListing(_))
        }
      }
    } ~
    path(Segment) { idStr =>
      get {
        complete {
          val optTask: OptionT[Task, ApiResponse] = for {
            id <- OptionT.fromOption[Task](Try(idStr.toInt).toOption)
            user <- OptionT(UserService.find(id).transact(dbXa))
          } yield ApiResponse.UserListing(List(user))

          optTask.getOrElse(ApiResponse.Error.userNotFound(idStr))
        }
      }
    }

  val curationRoute: Route =
    pathEnd {
      get {
        optionalSession(oneOff, usingCookies) { userId =>
          complete {
            CurationService
              .listInfo(userId)
              .transact(dbXa)
              .map(ApiResponse.CurationListing(_))
          }
        }
      } ~
      post {
        requiredSession(oneOff, usingCookies) { userId =>
          parameters(('name, 'privacy)) { (nameStr, privacyStr) =>
            val eitherT: ApiResult[Task, ApiResponse] = for {
              name <- Valid.nonEmptyString(nameStr, "name")
              privacy <- Valid.privacyLevel(privacyStr)
              task = CurationService.create(name, userId, privacy).transact(dbXa)
              curation <- EitherT.liftT(task)
            } yield ApiResponse.CurationCreated(curation)

            complete(eitherT.value)
          }
        }
      }
    } ~
    path(Segment) { idStr =>
      val optTask: OptionT[Task, Curation] = for {
        id <- OptionT.fromOption[Task](Try(idStr.toInt).toOption)
        curation <- OptionT(CurationService.findInfo(id).transact(dbXa))
      } yield curation

      onSuccess(optTask.value) {
        case None => complete(ApiResponse.Error.curationNotFound(idStr))
        case Some(curation) =>
          get {
            optionalSession(oneOff, usingCookies) { userId =>
              complete {
                CurationService
                  .findEntriesForUser(curation, userId)
                  .transact(dbXa)
                  .map {
                    case None => ApiResponse.Error.curationNotFound(idStr)
                    case Some(entries) => ApiResponse.CurationDisplay(curation, entries)
                  }
              }
            }
          } ~
          post {
            requiredSession(oneOff, usingCookies) { owner =>
              parameters(('link.as[String], 'name.?, 'description.?)) { (link, nameParam, descParam) =>
                val ifAllowedToCreateEntry: ApiResult[Task, Unit] =
                  EitherT.cond(
                    curation.owner == owner, (),
                    ApiResponse.Error.notAllowed)

                val result: ApiResult[Task, ApiResponse] = for {
                  _ <- ifAllowedToCreateEntry
                  _ <- Valid.isUrl(link)
                  name <- Valid.optional(nameParam)(Valid.nonEmptyString(_, "name"))
                  desc <- Valid.optional(descParam)(Valid.nonEmptyString(_, "description"))
                  task = CurationService.addEntry(curation, link, name, desc).transact(dbXa)
                  entry <- EitherT.liftT(task)
                } yield ApiResponse.CurationEntryAdded(entry)

                complete(result.value)
              }
            }
          }

      }
    }
}
