package sharedex.server

case class AppConfig(server: ServerConfig, db: DatabaseConfig)

case class ServerConfig(
  host: String = "localhost",
  port: Int = 8080,
  secret: Option[String] = None
)

case class DatabaseConfig(
  host: String = "localhost",
  name: Option[String] = None,
  port: Int = 5432,
  user: String = "postgres",
  password: String = "",
  automigrate: Boolean = false)
