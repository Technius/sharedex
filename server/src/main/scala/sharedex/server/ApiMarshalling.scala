package sharedex.server

import akka.http.scaladsl.model._
import akka.http.scaladsl.marshalling._
import io.circe.syntax._
import io.circe.generic.auto._

import sharedex.models._

trait ApiMarshalling {
  def apiResponseTEM: ToEntityMarshaller[ApiResponse] =
    Marshaller.StringMarshaller.wrap(MediaTypes.`application/json`)(_.asJson.noSpaces)

  def apiResponseCode(r: ApiResponse): StatusCode = r match {
    case _: ApiResponse.UserLoggedIn => StatusCodes.OK
    case _: ApiResponse.UserListing => StatusCodes.OK
    case _: ApiResponse.UserCreated => StatusCodes.Created
    case ApiResponse.UserLoggedOut => StatusCodes.OK
    case _: ApiResponse.CurationListing => StatusCodes.OK
    case _: ApiResponse.CurationDisplay => StatusCodes.OK
    case _: ApiResponse.CurationCreated => StatusCodes.Created
    case _: ApiResponse.CurationEntryAdded => StatusCodes.Created
    case ApiResponse.Error(_, details) => apiErrorResponseCode(details)
  }

  def apiErrorResponseCode(errorDetails: ApiResponse.ErrorType) = {
    import ApiResponse.ErrorType._
    errorDetails match {
      case _: UserNotFound => StatusCodes.NotFound
      case _: CurationNotFound => StatusCodes.NotFound
      case _: InvalidRequest => StatusCodes.BadRequest
      case NotAllowed => StatusCodes.Forbidden
      case LoginFailed => StatusCodes.Unauthorized
    }
  }

  implicit def apiResponseTRM: ToResponseMarshaller[ApiResponse] =
    PredefinedToResponseMarshallers
      .fromStatusCodeAndHeadersAndValue(apiResponseTEM)
      .compose(apiR => (apiResponseCode(apiR), List.empty, apiR))

}
