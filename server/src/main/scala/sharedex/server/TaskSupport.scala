package sharedex.server

import akka.http.scaladsl.server._
import akka.http.scaladsl.marshalling._
import fs2.Task
import scala.util.Try

trait TaskSupport {

  implicit def taskMarshaller[A, B](implicit m: Marshaller[A, B]): Marshaller[Task[A], B] =
    Marshaller { implicit ec => t =>
      t.unsafeRunAsyncFuture.flatMap(m(_))
    }

  def onComplete[A](task: Task[A]): Directive1[Try[A]] =
    Directives.onComplete(task.unsafeRunAsyncFuture())

  def onSuccess[A](task: Task[A]): Directive1[A] =
    Directives.onSuccess(task.unsafeRunAsyncFuture())
}
