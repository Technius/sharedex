package sharedex.services

import cats.Applicative
import cats.implicits._
import cats.data.EitherT
import fs2.Task
import fs2.interop.cats._
import scala.language.higherKinds
import scala.util.Try

import sharedex.models._
import ApiResponse.Error

object Combinators {
  def nonEmptyString(data: String, keyname: String = "key"): ApiResult[Task, String] = {
    val trimmed = data.trim
    EitherT.cond(
      !trimmed.isEmpty, trimmed,
      Error.invalidRequest(keyname + " cannot be empty"))
  }

  def privacyLevel(privacy: String): ApiResult[Task, PrivacyLevel] =
    EitherT.fromOption(
      PrivacyLevel.fromString(privacy),
      Error.invalidRequest("No such privacy level: " + privacy))

  def userId(str: String): ApiResult[Task, Int] =
    EitherT.fromOption(
      Try(str.toInt).toOption,
      Error.userNotFound(str))

  def isUrl(link: String): ApiResult[Task, Boolean] =
    EitherT.cond(
      Try(new java.net.URL(link)).isSuccess, true,
      Error.invalidRequest("Invalid url"))

  /**
    * If the option is defined, run the function on the value contained in the
    * option, producing an ApiResult (which may be a Left, ending the
    * computation). Otherwise, return Right(None) and proceed with the
    * computation chain.
    *
    * This is useful for validating optional parameters.
    *
    * @param optionA The option containing the value.
    * @param f The function to run on the value contained in the Option
    */
  def optional[A, B](optionA: Option[A])(f: A => ApiResult[Task, B]): ApiResult[Task, Option[B]] = {
    val default: ApiResult[Task, Option[B]] = EitherT.pure(None)
    val applied = optionA.map[ApiResult[Task, Option[B]]](a => f(a).map(Some(_)))
    applied.getOrElse(default)
  }
}
