package sharedex.services

import doobie.imports._
import sharedex.models._

trait UserQueries {
  def queryUsers: Query0[User] = sql"select * from users".query[User]
  def queryUserId(id: Int): Query0[User] =
    sql"select * from users where id = $id".query[User]
  def queryInsert(username: String): Update0 =
    sql"insert into users (username) values ($username)".update

  def queryUpdateUsername(id: Int, newUsername: String): Update0 =
    sql"update users set username = $newUsername where id = $id".update
}

trait UserService {
  def list: ConnectionIO[List[User]]

  def create(username: String): ConnectionIO[User]

  def find(id: Int): ConnectionIO[Option[User]]

  def changeUsername(id: Int, newUsername: String): ConnectionIO[Int]
}

object UserService extends UserService with UserQueries {

  override def list = queryUsers.list

  override def create(username: String) =
    queryInsert(username).withUniqueGeneratedKeys("id", "username")

  override def find(id: Int) =
    queryUserId(id).option

  override def changeUsername(id: Int, newUsername: String) =
    queryUpdateUsername(id, newUsername).run
}
