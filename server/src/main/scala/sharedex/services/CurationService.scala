package sharedex.services

import doobie.imports._
import cats.data.OptionT
import cats.implicits._

import sharedex.models._

trait CurationQueries {
  def fragInfo = fr"select * from curations"

  def queryInfo: Query0[Curation] = fragInfo.query[Curation]

  def fragUserAccessPrivate(user: Int) =
    fr"(privacy = ${PrivacyLevel.Private.id} and owner = $user)"

  def queryInfoForUser(userOpt: Option[Int]): Query0[Curation] = {
    val selectPublic = fragInfo ++ fr"where privacy = ${PrivacyLevel.Public.id}"
    val userCurations =
      userOpt.map(user => fr"or" ++ fragUserAccessPrivate(user)).getOrElse(fr"")
    (selectPublic ++ userCurations).query[Curation]
  }

  def queryEntry: Query0[(Int, CurationEntry)] =
    sql"select * from curation_entries".query[(Int, CurationEntry)]

  def queryFindInfo(id: Int): Query0[Curation] =
    (fragInfo ++ fr"where id = $id").query[Curation]

  def queryFindEntry(id: Int): Query0[CurationEntry] =
    sql"select link, name, description from curation_entries where id = $id"
      .query[CurationEntry]

  def queryListAll: Query0[(Curation, CurationEntry)] =
    sql"""
        select c.*, e.link, e.name, e.description
        from curations c
        right outer join curation_entries e
        on c.id = e.id
      """.query[(Curation, CurationEntry)]

  def queryInsertInfo(name: String, owner: Int, privacy: PrivacyLevel): Update0 =
    sql"""
      insert into curations (name, owner, privacy)
      values ($name, $owner, ${privacy.id})""".update

  def queryInsertEntry(curationId: Int, link: String, name: Option[String],
                       desc: Option[String]): Update0 =
    sql"""
      insert into curation_entries (id, link, name, description)
      values ($curationId, $link, $name, $desc)""".update
}

trait CurationService {

  def listInfo(user: Option[Int] = None): ConnectionIO[List[Curation]]

  def listAll: ConnectionIO[Map[Curation, List[CurationEntry]]]

  def findInfo(id: Int): ConnectionIO[Option[Curation]]

  def findEntries(id: Int): ConnectionIO[List[CurationEntry]]

  def findEntriesForUser(curation: Curation, userId: Option[Int]):
      ConnectionIO[Option[List[CurationEntry]]]

  def findEntriesForUser(curation: Curation, userId: Int):
      ConnectionIO[Option[List[CurationEntry]]] =
    findEntriesForUser(curation, Some(userId))

  def addEntry(curation: Curation, link: String, name: Option[String],
               description: Option[String]): ConnectionIO[CurationEntry]

  def create(name: String, owner: Int, privacy: PrivacyLevel): ConnectionIO[Curation]
}

object CurationService extends CurationService with CurationQueries {

  override def listInfo(user: Option[Int]) =
    user.map(userId => queryInfoForUser(user)).getOrElse(queryInfo).list

  override def listAll =
    queryListAll.list.map(_.foldMap(t => Map(t._1 -> List(t._2))))

  override def findInfo(id: Int) =
    queryFindInfo(id).option

  override def findEntries(id: Int) =
    queryFindEntry(id).list

  override def findEntriesForUser(curation: Curation, userId: Option[Int]) =
    if (curation.isViewableBy(userId))
      findEntries(curation.id).map(Some(_))
    else
      (None: Option[List[CurationEntry]]).pure[ConnectionIO]

  override def create(name: String, owner: Int, privacy: PrivacyLevel) =
    queryInsertInfo(name, owner, privacy)
      .withUniqueGeneratedKeys("id", "name", "owner", "privacy")

  override def addEntry(curation: Curation, link: String, name: Option[String],
                        description: Option[String]): ConnectionIO[CurationEntry] =
    queryInsertEntry(curation.id, link, name, description)
      .withUniqueGeneratedKeys("id", "link", "name", "description")
}
