package sharedex.models

import cats.Monoid
import scala.collection.immutable.Seq

sealed trait ApiResponse
object ApiResponse {
  case class UserLoggedIn(user: User) extends ApiResponse
  case class UserCreated(user: User) extends ApiResponse
  case class UserListing(results: List[User]) extends ApiResponse
  case object UserLoggedOut extends ApiResponse

  case class CurationCreated(meta: Curation) extends ApiResponse
  case class CurationEntryAdded(entry: CurationEntry) extends ApiResponse
  case class CurationListing(results: List[Curation]) extends ApiResponse
  case class CurationDisplay(curation: Curation, entries: List[CurationEntry]) extends ApiResponse

  case class Error(reason: String, details: ErrorType) extends ApiResponse
  object Error {
    import ErrorType._

    def invalidRequest(errors: String*): Error =
      Error("Invalid request", InvalidRequest(errors.toList))

    def userNotFound(id: String): Error =
      Error("User not found", UserNotFound(id))

    def loginFail: Error =
      Error("Incorrect username or password", LoginFailed)

    def curationNotFound(id: String): Error =
      Error("Curation not found", CurationNotFound(id))

    def notAllowed: Error =
      Error("Not allowed", NotAllowed)
  }

  sealed trait ErrorType
  object ErrorType {
    case class InvalidRequest(errors: List[String]) extends ErrorType
    case class UserNotFound(id: String) extends ErrorType
    case class CurationNotFound(id: String) extends ErrorType
    case object NotAllowed extends ErrorType
    case object LoginFailed extends ErrorType

    // InvalidRequest is basically a List[String], so it follows that since List
    // is a monoid with respect to concat, so is InvalidRequest
    implicit val monoidForInvalidRequest = new Monoid[InvalidRequest] {
      def empty: InvalidRequest = InvalidRequest(List.empty)

      def combine(l: InvalidRequest, r: InvalidRequest) =
        InvalidRequest(l.errors ++ r.errors)
    }
  }
}
