package sharedex.models

case class Curation(
    id: Int,
    name: String,
    owner: Int,
    privacy: PrivacyLevel) {

  def isPublic: Boolean = privacy == PrivacyLevel.Public

  def isPrivate: Boolean = privacy == PrivacyLevel.Private

  def isViewableBy(user: Option[Int]) =
    isPublic || (isPrivate && user.map(_ == owner).getOrElse(false))
}

case class CurationEntry(
  link: String,
  name: Option[String] = None,
  description: Option[String] = None)

final case class PrivacyLevel private(id: String) extends AnyVal
object PrivacyLevel {
  val Public = PrivacyLevel("public")
  val Private = PrivacyLevel("private")

  def fromString(s: String): Option[PrivacyLevel] = s match {
    case "public" => Some(Public)
    case "private" => Some(Private)
    case _ => None
  }
}
