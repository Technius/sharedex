package sharedex

import cats.data.EitherT
import scala.language.higherKinds

package object models {
  type ApiResult[F[_], A] = EitherT[F, ApiResponse.Error, A]
}
