name := """sharedex"""

version := "1.0-SNAPSHOT"

lazy val root =
  (project in file("."))
    .settings(
      inThisBuild(List(
        scalaVersion := "2.12.2",
        scalacOptions ++= Seq(
          "-deprecation",
          "-feature",
          "-unchecked",
          "-Xlint:-unused,_",
          "-Xfatal-warnings",
          "-Xfuture",
          "-Yno-adapted-args",
          "-Ypartial-unification"
        )
      ))
    )
    .aggregate(server, client)

lazy val client =
  (project in file("client"))
    .settings(
      name := "sharedex-client",
      libraryDependencies ++= Seq(
        "org.scala-js" %%% "scalajs-dom" % "0.9.1"
      ),
      scalaJSUseMainModuleInitializer := true
    )
    .enablePlugins(ScalaJSPlugin, ScalaJSWeb)

lazy val server =
  (project in file("server"))
    .settings(
      name := "sharedex-server",
      fork in run := true,
      fork in test := true,
      compile in Compile := ((compile in Compile) dependsOn scalaJSPipeline).value,
      libraryDependencies ++= Seq(
        "com.typesafe.akka" %% "akka-http" % "10.0.4",
        "com.typesafe.akka" %% "akka-http-testkit" % "10.0.4" % Test,
        "com.softwaremill.akka-http-session" %% "core" % "0.4.0",
        "org.typelevel" %% "cats" % "0.9.0",
        "org.tpolecat" %% "doobie-core-cats" % "0.4.1",
        "org.tpolecat" %% "doobie-scalatest-cats" % "0.4.1" % Test,
        "org.postgresql" % "postgresql" % "9.4-1206-jdbc41",
        "org.flywaydb" % "flyway-core" % "4.0",
        "com.github.pureconfig" %% "pureconfig" % "0.7.0",
        "io.circe" %% "circe-core" % "0.8.0",
        "io.circe" %% "circe-generic" % "0.8.0",
        "io.circe" %% "circe-parser" % "0.8.0"
      ),
      scalaJSProjects := Seq(client),
      pipelineStages in Assets := Seq(scalaJSPipeline),
      pipelineStages := Seq(uglify, digest, gzip),
      WebKeys.packagePrefix in Assets := "public/",
      WebKeys.exportedMappings in Assets ++=
        (mappings in Assets).value map { case (file, path) =>
          file -> ((WebKeys.packagePrefix in Assets).value + path)
        }
    )
    .enablePlugins(SbtWeb)

// hack to load default project
onLoad in Global := (onLoad in Global).value andThen (Command.process("project server", _))
